/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Daniel Jimenez
 */
public class SqlConnection {
    public static Connection getConnection(){
        String URL = "jdbc:sqlserver://DESKTOP-D433VEA:1433; databaseName=FerreteriaDB;";
        
        try{
            Connection conn = DriverManager.getConnection(URL, "sa", "12345");
            return conn;
        }catch(SQLException e){
            System.out.println("No se pudo establecer la conexion a la base de datos: "+e.toString());
            return null;
        }
    }
}
