/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import controller.Controller;
import views.Login;
import views.Menu;

/**
 *
 * @author Daniel Jimenez
 */
public class Main {
    public static void main(String[] args) {
        Login session = new Login();
        Menu menu = new Menu();
        
        Controller controller = new Controller(session, menu);
        controller.openLogin();
    }
}
