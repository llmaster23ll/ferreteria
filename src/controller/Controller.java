/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.event.ActionEvent;
import views.Login;
import views.Menu;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Daniel Jimenez
 */
public class Controller implements ActionListener {
    private Login session;
    private Menu menu;
    
    public Controller(Login session, Menu menu){
        this.session = session;
        this.menu = menu;
        
        this.session.btnEnter.addActionListener(this);
    }
    
    public void openLogin(){
        session.setLocationRelativeTo(null);
        session.txtUser.requestFocus();
        session.setTitle("Iniciar Sesión - Ferreteria El Guerrero");
        session.pack();
        session.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        session.setLocationRelativeTo(null);
        session.setVisible(true);
    }
    
    public void openMenu() {
        menu.setExtendedState(MAXIMIZED_BOTH);
        menu.setTitle("Menú - Ferreteria El Guerrero");
        menu.pack();
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.setLocationRelativeTo(null);
        menu.setVisible(true);
        //session.setVisible(false);
        session.dispose();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (session.btnEnter == e.getSource()) {
            openMenu();
        }
    }
}